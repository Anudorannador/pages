# 椭圆曲线数字签名算法

[椭圆数字签名 (Elliptic Curve Digital Signature Algorithm, ECDSA)](https://en.wikipedia.org/wiki/Elliptic_Curve_Digital_Signature_Algorithm) 实际上是结合了[椭圆曲线 EC](https://en.wikipedia.org/wiki/Elliptic_curve)和[数字签名 DSA](https://en.wikipedia.org/wiki/Digital_Signature_Algorithm) 来产生的一种签名算法。签名算法，顾名思义，只能用于签名，而不适用于加密。

## 必要知识

- [逆元](https://en.wikipedia.org/wiki/Modular_multiplicative_inverse)
- [无穷远点](https://en.wikipedia.org/wiki/Point_at_infinity)
- [椭圆曲线 EC](https://en.wikipedia.org/wiki/Elliptic_curve)；
- [数字签名 DSA](https://en.wikipedia.org/wiki/Digital_Signature_Algorithm)。

## 签名参数

- $`\text{CURVE}`$：椭圆曲线及其域、方程式。比如 $`y^2 = x^3 + 7`$；

- $`G`$：基点 (base point)，比如 $`y^2 = x^3 + 7`$ 上的某点 $`(x_{0}, y_{0})`$；

- $`n`$：整数 order，必须是质数，而且满足 $`n \times G = 0`$（$`0`$ 表示无限远点）。

    - 另外定义 $`L_n`$ 表示 $`n`$ 的二进制位数，比如 $`5`$ 的二进制是 $`101`$，那么 $`L_5`$ 的值就是 $`3`$；

    - 实际上

- $`H`$：哈希 (hash) 函数，将信息 $`m`$ 哈希之后的结果可表示为 $`H(m)`$。常用的哈希算法 SHA-2。

## 算法过程

### 密钥和公钥

- 随机在 $`[1, n-1]`$ 中挑选出一个整数 $`d_A`$ 作为密钥；

- 计算点 $`Q_A = d_A \times G`$，那么 $`Q_A`$ 便是公钥，实际上只要给出 $`Q_A`$ 的横坐标的值，带入曲线方程，计算出纵坐标的值，取正值就可以知道整个公钥了。

### 签名

1. 计算 $`e = H(m)`$；

2. 令 $`z`$ 等于 $`e`$ 的二进制左起 $`L_n`$ 位，比如 $`e`$ 的二进制是 $`10111110`$，$`L_n`$ 的值是 $`3`$，那么 $`z`$ 的值就是 $`101`$；

3. 随机在 $`[1, n-1]`$ 中挑选出整数 $`k`$；

4. 计算点 $`(x_1, y_1) = k \times G`$；

5. 计算 $`r = x_1\mod n`$，如果 $`r=0`$，返回第3步；

6. 计算 $`s = k^{-1} (z + r \cdot d_A) \mod n`$，如果 $`s=0`$，返回第3步；

7. 签名就是元组 $`(r, s)`$。

### 验证签名

#### 验证公钥 $`Q_A`$

公钥 $`Q_A`$ 的验证，必须满足以下条件：

1. $`Q_A`$ 不是无限远点（这表示过 $`Q_A`$ 作切线与曲线没有其他交点）；

2. $`Q_A`$ 是曲线上的点；

3. $`n\times Q_A = 0`$（令 $`d_{\delta} = n - d_A`$， 那么 $`d_A \times G = (n-d_{\delta}) \times G = n\times G + d_{\delta} \times G`$ $`n\times G`$ 的结果是无限远点，所以 $`0 + d_{\delta}\times G = 0`$，这是我自己证明的，不知道正确与否）。

#### 验证签名内容

1. $`r, s`$ 的值要在 $`[1, n-1]`$ 范围内，否则验证失败；

2. 计算 $`e = H(m)`$；

3. 计算 $`z`$；

4. 计算 $`w=s^{-1} \mod n`$；

5. 计算 $`u_1 = zw \mod n`$ 和 $`u_2 = rw \mod n`$；

6. 计算点 $`C(x_1, y_1) = u_1\times G + u_2 \times Q_A`$，如果 $`(x_1, y_1) = 0`$，那么签名是无效的；

7. 如果 $`r \equiv x_1 \pmod n`$，那么是有效签名，否则无效。

### 正确性证明

```math
\begin{aligned}
&\because   & Q_A    & =  d_A \times G \\
&\therefore & C      & = u_1 \times G + u_2d_A\times G \\
&           &        & = (u_1+u_2d_A) \times G \\
&\therefore & x_1    & \equiv (u_1 + u_2 d_A) \times x_0 \\
&           &        & \equiv (zs^{-1} + rd_As^{-1}) \times x_0 && \text{（根据验证签名内容第5步和第4步）}\\
&           &        & \equiv (z + rd_A)s^{-1} \times x_0 & \pmod n \\
&\because   & s      & \equiv k^{-1} (z + r \cdot d_A) \mod n \\
&\therefore & s^{-1} & \equiv k(z + r \cdot d_A)^{-1} \mod n \\
&\therefore & x_1    & \equiv (z + rd_A)s^{-1} \times x_0 \\
&           &        & \equiv (z + rd_A)\times k(z + rd_A)^{-1} \times x_0 & & \text{（根据签名过程第6步）} \\
&           &        & \equiv [(z + rd_A)(z + rd_A)^{-1}] \cdot k \times x_0  \\
&           &        & \equiv k \times x_0 \\
&           &        & \equiv r & \pmod n \\
\end{aligned}
```

## 安全性

- 跟 DSA、盖默尔签名一样，$`k`$ 值每次都不能一样，否则的话 $`r`$ 的值会相同，多次相同结合多项式是可以反向计算出 $`d_A`$。一个典型的例子就是 2010 年 10 月索尼 PlayStation 3 (PS3) 的签名私钥被爆了出来。