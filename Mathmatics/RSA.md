# RSA

## 简介

RSA 算法是[麻省理工(MIT)](https://en.wikipedia.org/wiki/Massachusetts_Institute_of_Technology) 的 [Ron Rivest](https://en.wikipedia.org/wiki/Ron_Rivest)、[Adi Shamir](https://en.wikipedia.org/wiki/Adi_Shamir) 和 [Leonard Adleman](https://en.wikipedia.org/wiki/Leonard_Adleman) 最早于 1978 年提出的，RSA 是这三位提出者的姓氏的第一个字母的缩写，有关更多 RSA 的信息，请参考[维基百科](https://en.wikipedia.org/wiki/RSA_(cryptosystem))。

本文主要阐述利用 RSA 算法如何产生公钥、私钥，如何加密、解密以及证明其正确性，说明其安全性。

值得注意的是，RSA 最开始的原始论文使用的是 [欧拉定理(Euler's theorem)](https://en.wikipedia.org/wiki/Euler%27s_theorem) 进行计算公钥和私钥，但是在数学、计算机科学发展过程中，发现可以使用 [卡迈克尔函数 (Carmicheal function)](https://en.wikipedia.org/wiki/Carmichael_function) 进行优化，本文先讲述最开始的 RSA 算法，之后再介绍卡迈克尔是如何被引进以及如何优化 RSA 算法的。

### 必要知识

在阅读下文时，你需要掌握如下基础知识：

- [同余的性质](https://en.wikipedia.org/wiki/Congruence_relation)

- [卡迈克尔函数($`\lambda(n)`$)](https://en.wikipedia.org/wiki/Carmichael_function)

- [欧拉函数($`\phi(n)`$)、欧拉定理](https://en.wikipedia.org/wiki/Euler's_totient_function)


## 算法过程

### 公钥、私钥产生过程：

1. 选出两个大质数 $`p`$ 和 $`q`$；
2. 计算该两个数的乘积 $`n = p \times q`$；
3. 计算欧拉函数 $`\phi(n) = (p-1)\times (q-1)`$；
4. 随机选择一个整数 $`d`$ 且满足 $`\text{gcd}(d, \phi(n)) = 1`$；
5. 计算 $`e`$，且满足 $`e\cdot d \equiv 1 \mod \phi(n)`$，即 $`b`$ 的逆元；
6. $`(n, e)`$ 就是公钥， $`(n, d)`$ 就是私钥。

### 加密和解密过程：

明文 $`m`$（$`m \leq n`$），是不公开的，加密后密文是 $`c`$，是公开传输的，则加密：

```math
c \equiv m^e \mod n
```

解密：

```math
m \equiv c^d \mod n
```

### 安全性

1. 解密时， $`c^d \mod n`$，其中密文 $`c`$ 和 $`n`$ 是公开的，要想得到 $`m`$ 那么必须得到 $`d`$；
2. 而根据公私钥产生的第5步知道 $`e \cdot d \equiv 1 \mod \phi(n)`$， $`e`$ 是公钥的一部分，是公开已知的，那么求出 $`\phi(n)`$ 就能求出 $`d`$；
3. 而 $`\phi(n) = (p-1)\times (q-1)`$，所以必须先得求出 $`p`$ 和 $`q`$；
4. 而 $`p`$ 和 $`q`$ 两个大质数（现在通常使用的是 1024 位（二进制））只能通过 $`n`$ 分解质因数来求的，分解大的质数需要的计算量非常大，这里引用一段普通高中数课程标准实验教科书数学选修4-6（初等数论初步，2005年初审通过）第44页中的一段描述：

> 我们知道，任意大于1的整数总是可以分解成一些素因数的乘积形式，这只不过理论上的结果。当整数很大时，这件事情具体做做起来非常困难。用现代最快速的分解算法，在大型计算机上分解一个大整数所需时间如表 3 所示：
>
>表3：
>
|整数的位数|必须操作数|所需时间|
|:--:|:--:|:--:|
|50|$`1.5\times 10^{10}`$|$`3.9`$ 小时|
|75|$`9.0\times 10^{12}`$|$`104`$ 天|
|100|$`2.3\times 10^{15}`$|$`74`$ 年|
|200|$`1.2\times 10^{23}`$|$`3.8\times 10^9`$ 年|
|300|$`1.3\times 10^{29}`$|$`4.9\times 10^{15}`$ 年|
|500|$`1.5\times 10^{39}`$|$`4.2\times 10^{17}`$ 年|

不过注意的是，上表中的位数是10进制，而且该教材比较原始，数据比较老，大型计算机的计算速度已经大大提高，虽然准确性不强，但仍有一定参考价值。

[维基百科](https://en.wikipedia.org/wiki/RSA_(cryptosystem)#Integer_factorization_and_RSA_problem)也有对此相关介绍：

>Multiple polynomial quadratic sieve (MPQS) can be used to factor the public modulus n. The time taken to factor 128-bit and 256-bit n on a desktop computer (Processor: Intel Dual-Core i7-4500U 1.80GHz) are respectively 2 seconds and 35 minutes.
>
|Bits|Time|
|:--:|:--:|
|128|Less than 2 seconds|
|192|16 seconds|
|256|35 minutes|
|260|1 hour|

这个数据准确，直接说使用因特尔 Dual-Core i7-4500U 1.8GHz 的CPU来测试。

### 正确性

把加密和解密的两个式子联合起来：

```math
\begin{aligned}
\because m& \equiv c^d & \mod n \\
c &\equiv m^e & \mod n \\
\implies m &\equiv {(m^e)}^d & \mod n \\
\implies m &\equiv m^{ed} & \mod n
\end{aligned}
```

所以我们只要证明 $`m \equiv m^{ed} \mod n`$ 成立就行了。又：

```math
\begin{aligned}
& \because  e \cdot d \equiv 1 \mod \phi(n) \\
& \therefore  e \cdot d  = k\cdot \phi(n) + 1
\end{aligned}
```

其中 $`k`$ 为某个整数，那么：

```math
\begin{aligned}
& m^{ed} & \mod n \\
= & m ^{1+k \cdot \phi(n)} & \mod n \\
= & [(m \mod n)((m^k)^{\phi(n)} \mod n)] & \mod n \\
\end{aligned}
```

由欧拉定理：$`a, b`$ 互质，则 $`a^{\phi(b)} \equiv 1 \mod b`$ 可知，把 $`m^k`$ 看做是 $`a`$，$`n`$ 看做是 $`b`$，那么：

```math
\begin{aligned}
&\because & & a^{\phi(b)} & \equiv 1 & \mod b \\
&\implies & & (m^k)^{\phi(n)} & \equiv 1 &  \mod n \\
&\therefore &  [(m \mod n)&((m^k)^{\phi(n)} \mod n)] & & \mod n \\
& = & [(m \mod n) & \cdot 1] & & \mod n \\
& = & (m \mod n) & & & \mod n \\
& = &  m & & & \mod n \\
\end{aligned}
```

这里有个问题，欧拉定理中 $`a, b`$ 互质是前提，当我们把 $`m^k`$ 看做是 $`a`$，$`n`$ 看做是 $`b`$ 时， $`m^k`$ 与 $`n`$ 互质吗？

要保证 $`m^k`$ 与 $`n`$ 互质，只要保证 $`m`$ 与 $`n`$互质就行。 $`n = p \times q`$，而 $`p, q`$ 都是质数，所以只要保证 $`m`$ 不是 $`p`$ 或 $`q`$ 的倍数就行。一种常见的方法就是使用 [Padding schemes](https://en.wikipedia.org/wiki/RSA_(cryptosystem)#Padding_schemes) 来对 $`m`$ 进行取值。

## 卡迈克尔函数

### 公私钥产生：

引入卡迈克尔函数之后，产生步骤如下：

1. 选出两个大质数 $`p`$ 和 $`q`$；
2. 计算该两个数的乘积 $`n = p \times q`$；
3. 计算卡迈克尔函数 $`\lambda(n) = \text{lcm}(\lambda(p), \lambda(q)) = \text{lcm}(p-1, q-1)`$；
4. 随机选择一个整数 $`e (1 < e < \lambda(n))`$ 而且与 $`\lambda(n)`$ 互质；
5. 计算 $`d`$，且满足 $`e\cdot d \equiv 1 \mod \lambda(n)`$，即 $`e`$ 的逆元；
6. $`(n, e)`$ 就是公钥， $`(n, d)`$ 就是私钥。

可以看到发生了两个变化：

1. 使用卡迈克尔函数 $`\lambda(n)`$ 代替了 $`\phi(n)`$；
2. 先计算的是公钥中的 $`(e)`$ 再计算私钥中的 $`(d)`$，顺序与原始 RSA 计算顺序相反。

为什么要用 $`\lambda(n)`$ 代替 $`\phi(n)`$ 是因为 $`\lambda(n)|\phi(n)`$ 总是成立的，可以减少运算量，包括先计算 $`(e)`$ 后计算 $`(d)`$，也是便于计算，相关证明见[维基百科](https://en.wikipedia.org/wiki/RSA_(cryptosystem)#Integer_factorization_and_RSA_problem#Key_generation)。

### 加密、解密
加密和解密过程与原 RSA 相同。