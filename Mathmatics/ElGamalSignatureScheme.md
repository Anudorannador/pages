# 盖默尔签名

## 简介

盖默尔签名方法是 1984 年 Taher Elgamal 提出来的，用于验证签名，不能用于加密。有关更多信息，请查看[维基百科](https://en.wikipedia.org/wiki/ElGamal_signature_scheme)。

### 必要知识

阅读本文时，你需要掌握如下知识：

1. [整数模 N 乘法群](https://en.wikipedia.org/wiki/Multiplicative_group_of_integers_modulo_n)的定义和性质；

2. [同余](https://en.wikipedia.org/wiki/Modular_arithmetic)的性质：如 $`a\cdot c \equiv b \mod n`$，则 $`c \equiv (a^{-1}\cdot b) \mod n`$；

3. [欧拉函数](https://en.wikipedia.org/wiki/Euler%27s_totient_function)（$`\phi(n)`$）及重要性质：

    - 若 $`p`$ 为质数，则 $`\phi(p) = p-1`$；

    - 对于两个整数 $`e`$ 和 $`f`$，任意正整数 $`n`$ 和 $`a`$， 且 $`a`$ 与 $`n`$ 互质，那么：

        ```math
        \begin{aligned}
        & \text{if} & e &\equiv f \pmod{\phi(n)} \\
        & \implies & a^e &\equiv a^f \pmod{n}
        \end{aligned}
        ```

## 算法过程

### 算法参数

- $`H`$ 代表一个单向哈希函数；
- $`p`$ 是一个大质数；
- $`g`$ 是一个小于 $`p`$ 的数，这个数是从 整数模 $`p`$ 乘法群中随机选出来的。

### 秘钥产生

- 随机选择私钥 $`x`$，满足 $`1 < x < p-2`$；
- 计算 $`y=g^x \mod p`$；
- 公钥就是 $`y`$，私钥就是 $`x`$。

### 签名过程

签名消息 $`m`$：

1. 选择一个随机数 $`k`$，且满足 $`1<k<p-1`$ 且 $`\gcd(k, p-1)=1`$；
2. 计算 $`r \equiv g^k \pmod p`$；
3. 计算 $`s \equiv ( H(m) - xr)k^{-1} \pmod {p-1}`$；
4. 如果 $`s = 0`$ 那么重新开始。

最后，$`(r,s)`$ 就是签名。

### 验证签名过程

1. 先检查 $`r`$ 和 $`s`$ 满足： $`0 < r < p`$ 且 $`0 < s < p-1`$；
2. $`g^{H(m)} \equiv y^rr^s \pmod p`$

### 正确性验证

签名时，因为

```math
s \equiv (H(m) - xr)k^{-1} \pmod{p-1}
```

根据同余的性质：

```math
\begin{aligned}
&\therefore & s\cdot k & \equiv H(m) - x\cdot r & \pmod{p-1} \\
&\therefore & H(m) & \equiv s\cdot k + x\cdot r & \pmod{p-1}
\end{aligned}
```

那么，再根据上一节必要知识中提到的欧拉函数的性质：

```math
\begin{aligned}
& \text{if} & e &\equiv f  \pmod{\phi(n)} \\
& \implies & a^e &\equiv a^f \pmod{n}
\end{aligned}
```

- 把 $`g`$ 看做 $`a`$；
- $`H(m)`$ 看做 $`e`$；
- 把 $`(s \cdot k + x\cdot r)`$ 看成 $`f`$；
- $`n`$ 就是质数 $`p`$，它一定与 $`(s\cdot k + x \cdot r)`$ 互质，且 $`\phi(p) = p-1`$，所以：

```math
\begin{aligned}
g^{H(m)} & \equiv g^{sk+xr} \\
& \equiv g^{sk} \cdot g^{xr} \\
& \equiv (g^x)^r (g^k)^s \\
& \equiv (y)^r (r)^s & \pmod p
\end{aligned}
```

## 安全性

1. 计算式 $`y=g^x \pmod p`$ 中，知道 $`x`$ 计算 $`y`$ 时很简单的，但是反过来，知道 $`y`$ 计算 $`x`$ 则非常困难，而且结果是不唯一的（比如 $`2^3 = 8, 8 \mod 5 = 3`$，而 $`2^7 = 128, 128 \mod 5 = 3`$，所以私钥 $`x`$ 有可能是 $`3`$ 也有可能是 $`5`$）；

2. 但是可以通过哈希碰撞，也就说 $`H(m) \equiv H(M) \pmod{p-1}`$ 来找（主要结合签名过程第3步来证明，此处省略证明过程），同样，找出哈希碰撞也非常困难；

3. 另外，每次签名选择的 $`k`$ 也不能相同，否则每次签名 $`r`$ 是一样的，然后通过多组 $`s`$ 就能反推出私钥（证明过程略）。