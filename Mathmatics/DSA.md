# 数字签名算法

## 简介

[数字签名算法（Digital Signature Algorithm）](https://en.wikipedia.org/wiki/Digital_Signature_Algorithm)（以下简称 DSA） 顾名思义就是用于数字签名的，不用于加密，同样利用的是幂数取余，即 $`y=g^x \pmod p`$ 来作为安全保证。

DSA 是前 NSA 雇员 David W. Kravitz 于 1991 年 7 月 26 日刊登在 U.S. Patent 5,231,668，之后 NIST 将之公布于众。但是 Claus P. Schnorr 表示是他早就发表在了 U.S. Patent 4,995,082 上了。此事仍有争议。

1991 年 8 月，美国国家技术标准委员会（NIST）将 DSA 纳入到 **数字签名标准(Digital Signature Standard, DSS)** 之中，于 1994 整理至档案 FIPS 186，之后修订并于 1996 年发布 FIPS-1，2000 年发布 FIPS 186-2,2009 年发布 FIPS 186-3，直至当前最新的于 2013 年发布的 FIPS 186-4。

### 必要知识

- [盖默尔签名](https://en.wikipedia.org/wiki/ElGamal_signature_scheme)；

- [multiplicative order](https://en.wikipedia.org/wiki/Multiplicative_order)；

- [整数模$`n`$乘法群](https://zh.wikipedia.org/wiki/%E6%95%B4%E6%95%B0%E6%A8%A1n%E4%B9%98%E6%B3%95%E7%BE%A4)及其相关推论：

    如果关于 $`g`$ 取摸 $`p`$ 的一个 order 为 q，则对于正整数 $`e,f`$：

    ```math
    \begin{aligned}
    e & \equiv f & \mod q \\
    g^e & \equiv g^f &\mod p
    \end{aligned}
    ```

- [同余](https://en.wikipedia.org/wiki/Modular_arithmetic)的性质：如 $`a\cdot c \equiv b \mod n`$，则 $`c \equiv (a^{-1}\cdot b) \mod n`$。

## 算法过程

#### 参数产生

- 选择一个不可逆的加密哈希函数， 一般是 [SHA-1](https://en.wikipedia.org/wiki/SHA-1) 或者是 DSS 现在推行的更安全的 [SHA-2](https://en.wikipedia.org/wiki/SHA-2)；

- 选择钥匙的长度 $`L`$ 和 $`N`$，DSS 表示 $`L`$ 长度是 $`64`$ 的倍数，且 $`512 \leq L \leq 1024`$，但是 NIST 800-57 推荐应该为 2048 或者 3072。根据 FIPS 186-3，$`(L, N)`$ 的取值应当为 $`(1024, 160), (2048, 224), (2048, 256), (3076, 256)`$ 这几对中选取。另外 $`N`$ 的值必须哈希函数 $`H`$ 的输出内容的长度。

- 选择一个长度为 $`N`$-bit 的素数 $`q`$；

- 选择一个长度为 $`L`$-bit 的素数 $`p`$ 且 $`p-1`$ 是 $`q`$ 的倍数；

- 计算出 $`g`$，满足 $`\text{ord}_p(g) = q`$，即 $`g`$ 关于取模 $`p`$ 的 [multiplicative order](https://en.wikipedia.org/wiki/Multiplicative_order) 是 $`q`$， $`g^q\equiv 1 \pmod p`$。

最后，参数就是元组 $`(p, q, g)`$。

在计算 $`g`$ 的时候，可以利用欧拉函数的性质，选择随机选择一个数 $`h`$，且与 $`p`$ 互质，那么：

```math
h^{\phi(p)} = h^{(p-1) } \equiv 1 \pmod p \\
```

如果令 $`g^q = h^{p-1} \equiv 1 \pmod p`$，那么 $`g= \sqrt[q]{h^{p-1}} = h^{\frac{p-1}{q}}`$（这里也就是为什么选取参数 $`p`$ 时要求 $`p-1`$ 是 $`q`$ 的倍数原因之一）。然后 $`h`$ 一般使用2，直接带入即可算出 $`g`$。

而且可以利用反证法证明 $`\text{ord}_p(g)=q`$。令 $`q' < q`$ 且 $`g^{q'} \equiv 1 \pmod p`$，那么根据同余的性质：

```math
\begin{aligned}
&\because g^{q'} \equiv g^q \pmod p \\
&\therefore q' \equiv q \pmod{\phi(p)} \\
&\because p\text{是质数，} \therefore \phi(p) = p-1 \\
&\therefore q' \equiv q \pmod{p-1} \\
&\because (p-1)\text{是}q\text{的倍数，肯定大于等于}q\\
&\therefore q \mod p = q \\
&\because q' <q, (p-1)\text{肯定大于等于}q'\\
&\therefore q' \mod p = q' \\
&\therefore q' \mod{(p-1)} = q' =  q \mod{(p-1)} = q\\
&\therefore q' = q
\end{aligned}
```

这与假设的 $`q'<q`$ 矛盾，所以 $`\text{ord}_p(g)=q`$ 一定成立。$`p`$ 为质数的情况下，实际上只要保证 $`q \leq p-1`$，无论正整数 $`g`$ 的取值如何，都成立。

（上述反证法推论的部分是我自己证明出来的，并没有向任何人求教是否正确，尤其是最开始的两个步骤。）

### 公私钥生成

1. 随机选择私钥 $`x`$ 满足 $`0 < x < q`$；
2. 计算公钥 $`y = g^x \pmod p`$

### 签名
签名消息 $`m`$，哈希函数 $`H`$：

1. 选择一个随机数 $`k`$，且满足 $`1<k<q`$；
2. 计算 $`r = (g^k \mod p) \mod q`$；
3. 如果 $`r = 0`$ 回到第一步重新开始选择随机数；
4. 计算 $`s = (H(m) + xr)k^{-1} \pmod {q}`$；
5. 如果 $`s = 0`$ 那么重新开始。

最后，$`(r,s)`$ 就是签名。

### 验证签名

1. 先检查 $`r`$ 和 $`s`$ 满足： $`0 < r < q`$ 且 $`0 < s < q`$；
2. 计算 $`w = s^{-1} \mod q`$；
3. 计算 $`u_1 = H(m) \cdot w \mod q`$；
4. 计算 $`u_2 = r\cdot w \mod q`$；
5. 计算 $`v = (g^{u_1}y^{u_2} \mod p) \mod q`$；
6. 判断 $`v`$ 是否等于 $`r`$，如果相等，则签名通过。

### 正确性验证

因为：

```math
s = k ^{-1}(H(m) + xr) \mod q
```

根据同余性质可知：

```math
\begin{aligned}
k & \equiv H(m)s^{-1} + xrs^{-1} \\
  & \equiv H(m)w + xrw & \pmod q
\end{aligned}
```

因为 $`\text{ord}_p(g)=q`$， 所以 $`q`$ 是 $`g`$ 关于取摸 $`p`$ 的 orders 之一，所以根据 orders 具有的性质：

```math
\begin{aligned}
g^k & \equiv g^{H(m)w} g^{xrw} \\
    & \equiv g^{H(m)w} y^{rw} \\
    & \equiv g^{u_1}y^{u_2} & \pmod p
\end{aligned}
```

所以，签名正确的话，那么：

```math
\begin{aligned}
r &= (g^k \mod p) \mod q \\
  &= (g^{u_1}y^{u_2} \mod p) \mod q \\
  &= v
\end{aligned}
```

## 安全性

- 数字签名安全性依靠跟[盖默尔签名](https://en.wikipedia.org/wiki/ElGamal_signature_scheme)类似，也是根据 $`y= g^x \mod p`$ 已知 $`g,p,x`$ 算 $`y`$ 很容易，但是反过来根据 $`g,p,y`$ 计算 $`x`$ 很难，而且具有不唯一性；

- 随着参数 $`p,q`$ 的位数越大，安全性越高；

- 还是跟盖默尔签名一样，在签名的时候，如果使用的随机数 $`k`$ 多次相同的话，那么是可以结合哈希碰撞反推出私钥（证明略）。