# 椭圆曲线

## 必要知识

- [无限远点](https://en.wikipedia.org/wiki/Algebraically_closed_field#Other_properties)

## 基本定义

[椭圆曲线 (Elliptic-curve)](https://en.wikipedia.org/wiki/Elliptic_curve) 的定义为：

```math
y^2 = x^3 + ax + b
```

曲线的特点是没有[奇点](https://en.wikipedia.org/wiki/Singular_point_of_a_curve)的，或者说没有尖点，可以想象一下，一个三角形，三个角的顶点非常尖，所以三个顶点是奇点（尖点）。

以下列举了不同 $`a,b`$ 时，其椭圆曲线的大致样子（图来源[维基百科](https://upload.wikimedia.org/wikipedia/commons/d/db/EllipticCurveCatalog.svg_)）：

![椭圆曲线示例](./images/EllipticCurveCatalog.png)

## 运算

### "$`+`$" 运算

定义无穷远点 $`0`$ 为椭圆曲线 $`E`$ 上的一点。定义 $`+`$ 运算：$`E`$ 上的两点 $`P,Q`$：

1. 若该两点不重合，那么 $`P+Q`$ 表示穿过 $`P`$ 和 $`Q`$ 的直线和椭圆曲线相交的第三点，再经 $`x`$ 轴反射的镜像点 $`R`$；

    ![不重合](./images/ECCPQ1.png)

2. 若 $`P,Q`$ 是同一点，$`P+P=2P`$ 表示以 $`P`$ 为切点和椭圆曲线相交的点再经 $`x`$ 轴反射的镜像点 $`R`$；

    ![重合](./images/ECCPQ2.png)

    同理 $`3P = 2P + P`$：

    ![椭圆曲线3P](./images/ECCPQ3.png)

3. 若过 $`P, Q`$ 的直线（$`P, Q`$ 可以是不同点也可以是相同点）与y轴平行，$`P+Q=0`$（无限远点）。

另外也可以表示为（图源于[维基百科](https://en.wikipedia.org/wiki/Elliptic_curve#/media/File:ECClines.svg)）：

![椭圆曲线加法](./images/ECClines.png)

1. $`P+Q = -R, -R + R = 0`$ （无限远点）；

2. $`P+Q = -Q, -R + R = 0`$；

3. $`P+Q = 0`$，两点连线的直线只经过 $`P, Q`$ 两点，再无第三个点；

4. $`P+P = 0`$，过 $`P`$ 点做切线，再无与曲线相交的点。

### "$`\times`$" 运算

上述 $`+`$ 运算中， $`3P`$ 可以称之为 $`3\times P`$，所以可以简单理解 $`nP`$ 或者 $`n\times P`$ 是 $`P`$ 进行 $`n`$ 次加法操作，这跟我们熟悉的乘法类似。

## 一些特性

- 椭圆曲线，一定关于 $`x`$ 轴对称；

- 值得注意的是，任何一条直线与椭圆曲线相交，最多有3个点，在此不做证明，这也是 $`+`$ 运算的运算结果有唯一性的保证；

- **注意**：如果我们知道一个点 $`P`$ 的坐标，然后求 $`2P, 3P \cdots nP`$ 的坐标是比较简单的，但是如果给出 $`nP`$ 的坐标和 $`n`$ 的值，要求出 $`P`$ 的坐标值是非常困难的，只能枚举椭圆曲线上的点。这一条特性在利用椭圆曲线进行加密、签名时是非常重要的。

## 参考资料

- [维基百科-椭圆曲线](https://en.wikipedia.org/wiki/Elliptic_curve)；
- [维基百科-无限远点](https://en.wikipedia.org/wiki/Point_at_infinity)；
- [What is the math behind elliptic curve cryptography?](https://hackernoon.com/what-is-the-math-behind-elliptic-curve-cryptography-f61b25253da3)。