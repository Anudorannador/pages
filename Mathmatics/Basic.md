# 数学基础

## 目录

注意，本文指介绍相关定理和性质，**不**做证明！

- [同余](#同余)
- [模运算](#%E6%A8%A1%E8%BF%90%E7%AE%97-modulo-operation)
- [逆元](#%E9%80%86%E5%85%83-modular-multiplicative-inverse)
- [Multiplicative order](#Multiplicative-order)
- [欧拉函数](#%E6%AC%A7%E6%8B%89%E5%87%BD%E6%95%B0-eulers-totient-function)
- [费马小定理](#%E8%B4%B9%E9%A9%AC%E5%B0%8F%E5%AE%9A%E7%90%86-fermats-little-theorem)
- [卡迈克尔函数](#%E5%8D%A1%E8%BF%88%E5%85%8B%E5%B0%94%E5%87%BD%E6%95%B0-carmicheal-function)
- [快速幂相关](#%E5%BF%AB%E9%80%9F%E5%B9%82%E7%9B%B8%E5%85%B3)
- [无穷远点](%E6%97%A0%E9%99%90%E8%BF%9C%E7%82%B9)
- [代数闭域](%E4%BB%A3%E6%95%B0%E9%97%AD%E5%9F%9F)
- [维基百科相关链接](#%E7%BB%B4%E5%9F%BA%E7%99%BE%E7%A7%91%E9%93%BE%E6%8E%A5)

## 同余

### 定义

两个整数 $`a`$ 和 $`b`$，他们两个相除，得到的余数可以计作：

```math
a \% b
```

或

```math
a \mod b
```

若它们除以正整数 $`m`$ 所得的余数相等，则称 $`a`$，$`b`$ 对于模 $`m`$ 同余，记做：

```math
a \equiv b \mod m
```

有时候为了简写，也直接写作 $`a\equiv b`$，但是 $`\equiv`$ 这个符号还有恒等于的意思，所以在做出说明或者没有歧义时可以直接简写。

如果 $`a`$ 除以 $`b`$ 正好整除没有余数，可以计做 $`b|a`$。

### 性质

1. 如果 $`a \equiv b \mod m`$，那么 $`(a-b) \mod m = 0`$；
2. 如果 $`a_1 \equiv a_2 \mod n`$ 而且 $`b_1 \equiv b_2 \mod n`$，那么 $`(a_1 + b_1) \equiv (a_2 + b_2) \mod n`$。

## 模运算 (Modulo operation)

### 基本性质

1. $`(a \mod n) \mod n = a \mod n`$；
2. $`n^x \mod n = 0`$ 对于一切正整数 $`x`$ 均成立；
3. 如果 $`p`$ 是质数且 $`b`$ 不是 $`p`$ 的倍数，那么 $`ab^{p-1} \equiv a \mod p`$（因为费马小定理，见下文）。

### 分配率

1. $`(a + b) \mod n = (a \mod n + b \mod n) \mod n`$；
2. $`(a - b) \mod n = (a \mod n - b \mod n) \mod n`$；
3. $`(a \times b) \mod n = (a \mod n \times b \mod n) \mod n`$。

### 幂运算

1. $`a^b \mod n = [(a \mod n)^b]\mod n`$

## 逆元 (Modular multiplicative inverse)

### 定义

对于整数 $`a`$，其逆 $`x`$ 也是一个整数，且满足：

```math
ax \equiv 1 \mod m
```

我们称 $`x`$ 是 $`a`$ 关于 $`m`$ 的逆元，对于一个数 $`a`$，其逆可使用 $`a^{-1}`$ 表示。一般来说，$`m`$ 是质数。

另外，逆元也翻译作“模反元素”，这个翻译更靠近其英文名称 modular multiplicative inverse.

### 性质：

1. 如果 $`b, n`$ 互质，那么：
    ```math
    [(b^{-1} \mod n)(b \mod n)] \mod n = 1
    ```

    或

    ```math
    (b^{-1} \mod n)(b \mod n) \equiv 1 \mod n
    ```

2. 如果 $`b, n`$ 互质，那么对于正整数 $`a`$：
    ```math
    \frac{a}{b} \mod n = [(a \mod n)(b^{-1} \mod n)] \mod n
    ```

3. 若 $`a \equiv b \mod n`$ 且 $`a^{-1}`$ 存在，则 $`a^{-1} \equiv b^{-1} \mod n`$；

4. 若 $`ax \equiv b \mod n`$ 则 $`x \equiv a^{-1}b \mod n`$。

## Multiplicative order

这个我到现在没找到合适的中文，所以直接用其英语。

对于整数 $`a`$ 和一个正整数 $`n`$，如果 $`\gcd(a, n) = 1`$， 且 $`n`$ 的 multiplicative order 是 $`k`$ 的话， $`k`$ 必须满足：

1. $`k`$ 必须是正整数；
2. $`a^k \equiv 1 \pmod n`$；
3. $`k`$ 的值最小。

比如 $`(2^3=8) \equiv (2^7=128) \equiv (2^{11}=2048) \equiv 3 \pmod 5`$，但是 $`3`$ 是最小的，所以我们说 $`2`$ 关于取模 $`5`$ 的最小 multiplicative order 是 $`3`$。

Multiplicative order 可以计做 $`\text{ord}_n(a)`$ 或者 $`\Omicron_n(a)`$，上述的例子可以写作 $`\text{ord}_5(2) = 3`$。

值得注意的是， 如果没有限制 $`k`$ 的值最小，那么 $`k`$ 值将会有很多个，上述例子中， mulitiplicatiove order 是 $`3`$，但是还有 $`7, 11 ...`$ 等其他的 order。所有的 order 构成一个[整数模$`n`$乘法群](https://en.wikipedia.org/wiki/Multiplicative_group_of_integers_modulo_n)。

## 欧拉函数 (Euler's totient function)

定义：在数论中，欧拉函数使用 $`\phi(n)`$ 表示（其中 $`n`$ 为正整数），其含义是指小于 $`n`$ 的正整数且与 $`n`$ 互质的数的个数。比如 $`\phi(8) = 4`$，因为小于 8 的正整数中，只有 $`1, 3, 5, 7`$ 与 8 互质。

欧拉函数的英文叫 Euler's totient function, 也叫 Euler's phi function，因为使用的是希腊字符 [$`\phi`$ (phi)](https://en.wikipedia.org/wiki/Phi) 表示。

### 性质

1. 如果 $`p`$ 为质数，那么：

    ```math
    \phi(p) = p - 1
    ```

2. 如果 $`\gcd(m,n)=1`$，那么：

    ```math
    \phi(mn)=\phi(m)\phi(n)
    ```

3. 如果 $`p`$ 是质数，$`k \geqslant 1`$，则：

    ```math
    \phi(p^k) = p^{k-1}(p-1)=p^k(1-\frac{1}{p})
    ```

4. 任意两个整数 $`x`$ 和 $`y`$，对于任意正整数 $`n`$ 和 $`a`$，且 $`a`$ 与 $`n`$ 互质，那么：

    ```math
    \begin{aligned}
    & \text{if} & x &\equiv y  \pmod{\phi(n)} \\
    & \implies & a^x &\equiv a^y \pmod{n}
    \end{aligned}
    ```

### 欧拉定理

如果 $`a`$ 和 $`n`$ 互质，那么：

```math
a^{\phi(n)} \mod n = 1
```

或

```math
a^{\phi(n)} \equiv 1 \mod n
```

如果 $`n`$ 是质数，那么正好是费马小定理（见下文）。

## 费马小定理 (Fermat's little theorem)

### 定义

1. 如果 $`p`$ 是一个质数，那么对于任何一个整数 $`a`$， $`a^p - a`$ 一定是 $`p`$ 的倍数，写成数学表达式：

    ```math
    (a^p - a) \mod p = 0
    ```

    或

    ```math
    a^p \equiv a \mod p
    ```

    比如 $`a=2`$，$`p=7`$，那么 $`2^7=128`$，$`128-2=126`$，$`126`$ 是 $`p`$ 的 $`18`$ 倍，即 $`126 = 7 \times 18`$。

2. 如果 $`a`$ 不是 $`p`$ 的倍数，那么 $`a^{p-1} -1`$ 是 $`p`$ 的倍数，数学表达式为：

    ```math
    (a^{p-1} - 1) \mod p = 0
    ```

    或

    ```math
    a^{p-1} \equiv 1 \mod p
    ```

    比如 $`a=2`$，$`p=7`$，那么 $`2^{7-1}=64`$，$`64-1=63`$，$`63`$ 是 $`p`$ 的 $`9`$ 倍，即 $`63 = 7 \times 9`$。

## 卡迈克尔函数 (Carmicheal function)

### 定义

卡迈克尔函数计做 $`\lambda(n)`$（$`n`$属于正整数），对于任何正整数 $`a`$（$`1\leqslant a \leqslant n`$ 且与 $`n`$ 互质），$`a^{\lambda(n)}`$ 的值满足 $`a^{\lambda(n)} \equiv 1 \mod n`$。

比如 $`\lambda(8) = 2`$ ，因为小于 $`8`$ 且与 $`8`$ 互质的正整数 $`1,3,5,7`$，而且这四个数的 $`2`$ 次方都除以 $`8`$ 的余数都是 $`1`$，即：$`1^2 \equiv 3^2 \equiv 5^2 \equiv 7^2 \mod 8`$。

### 计算方法

```math
\lambda(n) = \left \{ 
    \begin{aligned}
    \phi(n) & & \text{for a power of an odd prime and for 2 and 4} \\
    \frac{\phi(n)}{2} & & \text{other cases}
    \end{aligned}
\right. 
```

## 快速幂相关

### 平方快速幂（Exponentiantion by squaring）

基本方法：

```math
x^n = \left\{
    \begin{aligned}
    & x(x^2)^\frac{n-1}{2} & \text{if } n \text{ is odd} \\
    & x(x^2)^\frac{n}{2} & \text{if } n \text{ is odd} 
    \end{aligned}
\right.
```

很明显，使用了二分法，将时间复杂度由 $`\Omicron(n)`$ 降到 $`\Omicron (\log_2 n)`$，可以使用递归或者递推方法算出。有关更多信息，请查看[维基百科](https://en.wikipedia.org/wiki/Exponentiation_by_squaring)。

### 快速幂取模（Modular exponentiation）

如下数学式：

```math
c = b^e \mod m
```

其中 $`b`$ 为基数（base），$`e`$ 为幂（exponent），$`m`$ 为模数。若 $`e`$ 为负数，那么根据欧几里得算法：$`c = b^e \mod m = d^{-e} \mod m`$，其中 $`d`$ 是 $`b`$ 的逆元。

#### 最小内存方法

这个方法就是字面意思，节省内存。该方法利用：

```math
\begin{aligned}
 & c & \mod m \\
= & b^e & \mod m \\
= & [(b^{e-1} \mod m) \times (b \mod m)] &\mod m \\
...
\end{aligned}
```

这是个递归式，改成递推算法则是：

1. 令 $`c=1, e'=0`$；
2. $`e' \leftarrow e'+1`$；
3. 计算 $`c \leftarrow (b\times c) \mod n`$；
4. 如果 $`e' < e`$，继续第2步计算；否则最终结果就是 $`c`$。

Python 代码示例：

```python
def modular_pow(base, exponent, modulus):
    if modulus == 1:
        return 0 
    c = 1
    for e_prime in xrange(0, exponent-1):
        c = (c * base) % modulus
    return c
```

看得出其计算复杂度跟直接算时一样的，仍然为 $`\Omicron(e)`$，但是不用开巨大内存去存放中间结果 $`b^e`$。

#### 从右至左二进制法
任何一个数，都能用二进制来表示，比如 $`5`$，其二进制是 $`110`$，则 $`5 = (1\times 2^2) + (1\times 2^1) + (0\times 2^0)`$。同样对于幂 $`e`$：

```math
e = \sum^{n-1}_{i=0}a_i2^i
```

其中 $`a_i`$ 要么为 $`0`$，要么为 $`1`$，$`0\leq i < n`$。那么：

```math
b^e = b ^{\left(\displaystyle{\sum^{n-1}_{i=0}a_i2^i}\right)} = \prod^{n-1}_{i=0}(b^{2^i})^{a_i}
```

看得出，如果 $`a_i`$ 为 $`0`$，那么因数 $`(b^{2^i})^{a_i}`$ 始终为 $`1`$；如果 $`a_i`$ 为 $`1`$，$`(b^{2^i})^{a_i} = b^{2^i}`$，而：
```math
\begin{aligned}
  & b^{2^i} \\
= & b^{2^{i-1}\times 2} \\
= & (b^{2^{i-1}})^2 \\
= & [(b^{2^{i-2}})^2]^2 \\
= &...
\end{aligned}
```

再结合 $`(a\cdot b)\mod n = (a \mod n)(b \mod n) \mod n`$：

```math
\begin{aligned}
  & \prod^{n-1}_{i=0}(b^{2^i})^{a_i} & \mod m \\
= & [(\prod^{n-2}_{i=0}(b^{2^i})^{a_i} \mod m)\cdot ((b^{2^{n-1}})^2) \mod m] & \mod m \\
= & ...
\end{aligned}
```

这是个递归的式子，有兴趣的读者可以尝试改为递推的式子。时间复杂度取决于 $`e`$ 二进制一共有多少位，也就是 $`\Omicron(\log_2e)`$。

有关更多的内容，请查看维基百科[快速幂取模](https://en.wikipedia.org/wiki/Modular_exponentiation)。

## 无限远点

在代数几何中 [无限远点](https://en.wikipedia.org/wiki/Point_at_infinity) 又称无穷远点、理想点 (ideal point)，是假设出来的点，表示的是每一条直线的“终点” ("end" of each line)。无穷远点用 $`0`$ 来表示，当然 $`-0`$ 和 $`0`$ 是等价的。

- 无限远点还可以看做是平行线相交的两个点；

- 无限远点 $`+,\times`$ 任何点都是无限远点。

## 代数闭域

### 域

[域 (filed)](https://en.wikipedia.org/wiki/Field_(mathematics)) 简而言之有域有两个部分构成：

1. 一个集合 (set)；
2. 为这个集合需要定义加、减、乘、除（除了除以零意外）的代数运算。

我们知道整数集合、有理数集合、实数集合甚至是复数集合，但是这些集合仅仅只是“数”的集合，如果我们对这些“数”的集合在定义了加、减、乘、除的操作，就是域。比如整数这个集合，再接合我们对整数进行定义的加、减、乘、除的四则运算操作，可以称之为整数域。

另外，可以简单理解一个集合以及对这个集合的操作构成了[代数结构(Algebraic structure)](https://en.wikipedia.org/wiki/Algebraic_structure)。

域属于[环 (ring)](https://en.wikipedia.org/wiki/Ring_(mathematics)) 的一种，在这里就不展开讲述了。

### 代数闭域

某个域 $`F`$ 如果是[代数闭域(Algebraically closed field)](https://en.wikipedia.org/wiki/Algebraically_closed_field)，那么利用这个域中的集合的“数”以及域的操作，构建出的任何单变量多项式（多项式次数大于零）在集合中要能找到根。

比如，实数域就不是代数闭域，因为构造出 $`x^2+1=0`$，其根不是在实数这个集合中，或者说无实根。而复数域则是代数闭域。

### 代数闭包和代数扩张

代数闭包和代数扩张都是相对概念，如果 $`E`$ 是一个代数闭域，而 $`E\supset F`$，那么我们称 $`E`$ 是 $`F`$ 的代数闭包，$`F`$ 的一个代数扩张是 $`E`$。

## 维基百科链接：

- [同余](https://en.wikipedia.org/wiki/Congruence_relation)
- [逆元](https://en.wikipedia.org/wiki/Modular_multiplicative_inverse)
- [模运算](https://en.wikipedia.org/wiki/Modulo_operation)
- [欧拉函数](https://en.wikipedia.org/wiki/Euler%27s_totient_function)
- [费马小定理](https://en.wikipedia.org/wiki/Fermat%27s_little_theorem)
- [卡迈克尔函数](https://en.wikipedia.org/wiki/Carmichael_function)
- [快速幂取模](https://en.wikipedia.org/wiki/Modular_exponentiation)
- [无限远点](https://en.wikipedia.org/wiki/Point_at_infinity)
- [域](https://en.wikipedia.org/wiki/Field_(mathematics))
- [代数闭域](https://en.wikipedia.org/wiki/Algebraically_closed_field)